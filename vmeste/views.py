import json

from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.db import connection
from django.http import JsonResponse
from .download import downloadFilm
from .search_video import searchVideo
from .search_external import searchExternal
from .models import *


class Home(TemplateView):
	def get(self, request, *args, **kwargs):
		rooms = Room.objects.all()
		all_films = Film.objects.filter(is_active=True, is_hidden=False)
		loaded_films = 16
		start = int(request.GET.get('start', 0))
		films = all_films[start:start + loaded_films]

		if start>0:
			return render(request, 'other_films.htm', locals())	

		cur = connection.cursor()
		cur.execute("select film_id, count(*) as x from vmeste_room r left join vmeste_film f on f.id=r.film_id where f.is_hidden=false and f.is_active=true and max_user_count > 0 and r.dt_create>=current_timestamp - interval '14 days' group by film_id order by x desc limit 12")

		popular_films = []
		for film_id in [c[0] for c in cur]:
			popular_films.append(Film.objects.get(pk=film_id))
		return render(request, self.template_name, locals())


class RoomView(TemplateView):
	def get(self, request, *args, **kwargs):
		film = Film.objects.get(slug=kwargs['film_id'])
		template_name = self.template_name
		if film.is_external:
			template_name = 'room_external.html'

		if 'room_id' not in kwargs.keys():
			r = Room.objects.create(film=film, title=film.title, is_open=request.GET.get('open', '0')=='1')
			request.session['author_uuid'] = r.author_uuid.hex
			r.room_uuid_hex = str(r.room_uuid.hex)
			r.user_agent = request.META['HTTP_USER_AGENT']
			r.ip = request.META['REMOTE_ADDR']
			r.save()
			return HttpResponseRedirect('/show/'+str(film.slug)+'/'+str(r.room_uuid.hex))

		room = Room.objects.get(room_uuid=kwargs['room_id'])
		is_author = request.session.get('author_uuid')==room.author_uuid.hex
		return render(request, template_name, locals())


class RoomExternalView(TemplateView):
	def get(self, request, *args, **kwargs):
		film = ExternalFilm.objects.get(token=kwargs['token'])

		if 'room_id' not in kwargs.keys():

			r = Room.objects.create(external_film=film, title=film.title, is_open=request.GET.get('open', '0')=='1')
			request.session['author_uuid'] = r.author_uuid.hex
			r.room_uuid_hex = str(r.room_uuid.hex)
			r.user_agent = request.META['HTTP_USER_AGENT']
			r.ip = request.META['REMOTE_ADDR']
			r.save()
			return HttpResponseRedirect('/view/'+str(film.token)+'/'+str(r.room_uuid.hex))

		room = Room.objects.get(room_uuid=kwargs['room_id'])
		is_author = request.session.get('author_uuid')==room.author_uuid.hex

		return render(request, self.template_name, locals())



class FilmView(DetailView):
	model = Film
	def get(self, request, *args, **kwargs):
		object = self.get_object()
		if object.is_hidden:
			return render(request, 'abuse.html', locals())
		return render(request, self.template_name, locals())


class FilmEditView(DetailView):
	model = Film
	def get(self, request, *args, **kwargs):
		object = self.get_object()
		hide_metrika = True
		return render(request, self.template_name, locals())

	def post(self, request, *args, **kwargs):
		object = self.get_object()

		object.title = request.POST.get('title')
		object.server = request.POST.get('server')
		object.slug = request.POST.get('slug')
		info = object.info
		info["orig_name"] = request.POST.get('orig_name').strip()
		info["slogan"] = request.POST.get('slogan').strip()
		info["year"] = request.POST.get('year').strip()
		info["country"] = request.POST.get('country').strip()
		info["genre"] = [d.strip() for d in request.POST.get('genre').strip().split(',')]
		info["producer"] = [d.strip() for d in request.POST.get('producer').strip().split(',')]
		info["actors"] = [d.strip() for d in request.POST.get('actors').strip().split(',')]
		info["timing"] = request.POST.get('timing').strip()
		info["description"] = request.POST.get('description').strip()
		info["torrent_url"] = request.POST.get('torrent_url').strip()
		info["url"] = request.POST.get('url').strip()

		object.info = info
		object.genre.set([Genre.objects.get(name=d.strip()) for d in request.POST.get('genre').strip().split(',')])

		object.save()

		return HttpResponseRedirect(object.get_absolute_url()+'edit')


class GenreView(DetailView):
	model = Genre
	def get(self, request, *args, **kwargs):
		object = self.get_object()
		rooms = Room.objects.all()
		all_films = Film.objects.filter(is_active=True, is_hidden=False, genre=object)
		loaded_films = 16
		start = int(request.GET.get('start', 0)		)
		films = all_films[start:start + loaded_films]

		if start>0:
			return render(request, 'other_films.htm', locals())	

		cur = connection.cursor()
		cur.execute("select r.film_id, count(*) as x from vmeste_room r join vmeste_film_genre fg on r.film_id=fg.film_id left join vmeste_film f on r.film_id=f.id where f.is_hidden=false and f.is_active=true and max_user_count > 0 and genre_id=%d and r.dt_create>=current_timestamp - interval '14 days' group by r.film_id order by x desc limit 12"%(object.pk))
		popular_films = []
		for film_id in [c[0] for c in cur]:
			popular_films.append(Film.objects.get(pk=film_id))
					
		return render(request, self.template_name, locals())


class SearchView(TemplateView):
	def get(self, request, *args, **kwargs):
		text = request.GET.get('text', '').strip().lower()

		if text=='':
			return render(request, self.template_name, locals())
		
		for s in Synonym.objects.all():
			text = text.replace(s.text1.lower(), s.text2.lower())

		films = Film.objects.filter(is_active=True, is_hidden=False)
		for w in text.split(' '):
			films = films.filter(title__icontains=w)

		if films.count()==0:
			films = Film.objects.filter(is_active=True, is_hidden=False)
			for w in text.split(' '):
				films = films.filter(info__icontains=w)
		rcount = films.count()

		results = searchExternal(text)
		if results==None: results = {}
		# if request.user.is_authenticated:
		# 	# results = ExternalSearch.objects.filter(text=text)
		# 	results = searchExternal(text)

		if request.user.is_staff:
			all_collections = FilmCollection.objects.filter(is_filling=True)

		rcount += len(results.get("movie", []))
		rcount += len(results.get("serial", []))

		if 'bot' not in request.META['HTTP_USER_AGENT'].lower() and not Search.objects.filter(text=text, dt_create__gte=datetime.datetime.now() - datetime.timedelta(hours=1)).order_by('-id').exists():
			Search.objects.create(text=text, rcount=rcount)

		return render(request, self.template_name, locals())


class ErrorView(TemplateView):
	def post(self, request, *args, **kwargs):
		request_body = request.body.decode('utf-8')		

		ErrorJS.objects.create(body=request_body)

		return JsonResponse({'status':'success'})   


class MetricsView(TemplateView):
	def post(self, request, *args, **kwargs):
		body = json.loads(request.body.decode('utf-8'))
		body['user_agent'] = request.META['HTTP_USER_AGENT']
		Metrics.objects.create(data=body)

		return JsonResponse({'status':'success'})   


class ChatView(TemplateView):
	def get(self, request, *args, **kwargs):
		rooms = []
		hide_metrika = True
		if request.GET.get('room')!=None:
			rooms = Room.objects.filter(room_uuid=request.GET.get('room'))
			users = UserInRoom.objects.filter(room=rooms[0]).order_by('-id')
			return render(request, self.template_name, locals())
			
		open_rooms = Room.objects.filter(user_count__gt=0, dt_create__gte=datetime.datetime.now()-datetime.timedelta(hours=4)).order_by("-id")
		last_rooms = Room.objects.filter(user_count=0, max_user_count__gt=1, dt_create__gte=datetime.datetime.now()-datetime.timedelta(hours=12)).order_by("-id")
		last_comments = FilmComment.objects.filter(is_hide=False, is_other_site=False).order_by("-id")[:5]
		last_feedbacks = Feedback.objects.all().order_by("-id")[:5]
		return render(request, self.template_name, locals())


class CommentView(DetailView):
	model = Film

	def get(self, request, *args, **kwargs):
		return HttpResponseRedirect(self.get_object().get_absolute_url())

	def post(self, request, *args, **kwargs):
		object = self.get_object()

		username = request.POST.get('username', '').strip()
		comment = request.POST.get('comment', '').strip()

		if username!='' and comment!='' and not FilmComment.objects.filter(comment=comment).exists():
			FilmComment.objects.create(film=object, username=username, comment=comment)

		return HttpResponseRedirect(object.get_absolute_url())


class StatView(TemplateView):
	def get(self, request, *args, **kwargs):
		films = []
		for d in Film.objects.filter(is_active=True, is_hidden=False).order_by('id'):
			films.append({
				'film': d,
				'rooms_count': Room.objects.filter(film=d).count()
				})
		films.sort(key = lambda x: x.get('rooms_count'))
		hide_metrika = True
		return render(request, self.template_name, locals())


class EnableFilmView(DetailView):
	model = Film
	def get(self, request, *args, **kwargs):
		film = self.get_object()
		if request.GET.get('hls')=='1':
			film.is_hls = True
			film.save()
			return JsonResponse({'status': 'success'})
			
		film.is_active = True
		film.save()
		return JsonResponse({'status': 'success'})


class AddFilmView(TemplateView):
	def get(self, request, *args, **kwargs):
		hide_metrika = True
		return render(request, self.template_name, locals())
		
	def post(self, request, *args, **kwargs):

		url = request.POST.get('url')
		torrent_url = request.POST.get('torrent_url')

		film = downloadFilm(url)
		if film:
			info = film.info
			info['torrent_url'] = torrent_url
			film.info = info
			film.save()
			return HttpResponseRedirect(film.get_edit_absolute_url())
		msg = 'Фильм не добавлен, возможно, что он уже существует в базе'
		hide_metrika = True
		return render(request, self.template_name, locals())


class GetConvertView(TemplateView):
	def get(self, request, *args, **kwargs):
		data = {}

		if request.GET.get('hls')=='1':
			for d in Film.objects.filter(is_hls=False, server='s1.vmeste.tv').order_by('-id'):
				data['id'] = d.pk
				break
			return JsonResponse(data)	

		if request.GET.get('user')=='1':
			if request.GET.get('no_convert')=='1':
				UserVideo.objects.filter(pk=request.GET.get('id')).update(is_process=False)
				return JsonResponse({'status': 'success'})

			if request.GET.get('start_convert')=='1':
				UserVideo.objects.filter(pk=request.GET.get('id')).update(dt_process_start=datetime.datetime.now())
				return JsonResponse({'status': 'success'})

			if request.GET.get('ready')=='1':
				UserVideo.objects.filter(pk=request.GET.get('id')).update(dt_process_end=datetime.datetime.now(), is_active=True, is_process=False)
				return JsonResponse({'status': 'success'})

			for d in UserVideo.objects.filter(is_active=False, is_process=False).order_by('id'):
				data['id'] = d.pk
				data['url'] = d.file.url
				data['filename'] = d.file_name
				d.is_process = True
				d.server = request.GET.get('server')
				d.save()
				break

			return JsonResponse(data)				

		for d in Film.objects.filter(is_active=False, server='s1.vmeste.tv').order_by('id'):
			if d.info.get('torrent_url', '')=='': continue
			data['torrent_url'] = d.info.get('torrent_url')
			data['id'] = d.pk
			data['title'] = d.title
			break



		return JsonResponse(data)


class MasterProgressView(TemplateView):
	def get(self, request, *args, **kwargs):
		r = Room.objects.get(room_uuid=kwargs['room_id'])
		data = {'progress': r.progress}
		return JsonResponse(data)


class FeedbackView(TemplateView):
	def get(self, request, *args, **kwargs):
		good = request.GET.get('good')
		return render(request, self.template_name, locals())

	def post(self, request, *args, **kwargs):
		text = request.POST.get('text', '')

		if text!='' and self.maySave(text):
			Feedback.objects.create(text=text)
		return HttpResponseRedirect('/feedback/?good=1')

	def maySave(self, text):
		phrases = ['cryptocurrenc', 'geld zu verdienen', 'schnell macht schnelles', 'fast money', 'maison - seulement', 'comment transformer', 'Win iPhone', 'won iPhone']
		for p in phrases:
			if p in text.lower():
				return False
		return True

class FilmCollectionView(DetailView):
	model = FilmCollection
	def get(self, request, *args, **kwargs):
		object = self.get_object()

		if request.user.is_staff:
			# print(request.GET.get('efilm'))
			# print(object.films.filter(id=request.GET.get('film')).exists())
			efilm_id = None
			if request.GET.get('efilm'):
				efilm_id = ExternalFilm.objects.get(token=request.GET.get('efilm')).pk

			film_id = None
			if request.GET.get('film'):
				film_id = request.GET.get('film')

			if film_id and not object.films.filter(id=film_id).exists():		
				print('adding film', film_id)		
				object.films.add(Film.objects.get(pk=film_id))
				object.save()
				return HttpResponseRedirect(object.get_absolute_url())
			
			if film_id and request.GET.get('rm')=='1' and object.films.filter(id=film_id).exists():				
				print('removing film', film_id)		
				object.films.remove(Film.objects.get(pk=film_id))
				object.save()
				return HttpResponseRedirect(object.get_absolute_url())

			if efilm_id and not object.films_external.filter(id=efilm_id).exists():				
				print('adding efilm', efilm_id)		
				object.films_external.add(ExternalFilm.objects.get(pk=efilm_id))
				object.save()
				return HttpResponseRedirect(object.get_absolute_url())
			
			if efilm_id and request.GET.get('rm')=='1' and object.films_external.filter(id=efilm_id).exists():				
				print('removing efilm', efilm_id)		
				object.films_external.remove(ExternalFilm.objects.get(pk=efilm_id))
				object.save()
				return HttpResponseRedirect(object.get_absolute_url())

		return render(request, self.template_name, locals())


class YoutubeView(TemplateView):
	def get(self, request, *args, **kwargs):
		if kwargs.get('video_id'):
			video = VideoLink.objects.get(pk=kwargs.get('video_id'))
			room = Room.objects.create(title=video.title, video_url=video.video_url, is_open=False)
			request.session['author_uuid'] = room.author_uuid.hex
			room.room_uuid_hex = str(room.room_uuid.hex)
			room.user_agent = request.META['HTTP_USER_AGENT']
			room.ip = request.META['REMOTE_ADDR']
			room.save()			
			return HttpResponseRedirect(room.get_absolute_url())

		if kwargs.get('room_id'):
			room = Room.objects.get(room_uuid=kwargs['room_id'])
			is_author = request.session.get('author_uuid')==room.author_uuid.hex
			return render(request, 'room.html', locals())

		url = request.GET.get('url',request.POST.get('url', '')).strip()
		# print('url=', url)
		last_videos = VideoLink.objects.all().order_by('-id')[:15]
		if 'bot' in request.META['HTTP_USER_AGENT'].lower() or url=='':
			return render(request, self.template_name, locals())
		is_search = True		
		searchVideo(url)
		data = VideoLink.objects.filter(source_link=url)
		if data.exists():
			last_pk = data[data.count()-1].pk
		return render(request, self.template_name, locals())

