#-*- encoding: utf-8 -*-
import os
import datetime
import json
import requests
import re
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from bs4 import BeautifulSoup
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from unidecode import unidecode
from django.template.defaultfilters import slugify

from vmeste.models import *

def cleanhtml(raw_html):
  cleanr = re.compile('<.*?>')
  cleantext = re.sub(cleanr, '', raw_html)
  return cleantext

def infoData(txt):
    if txt==None: return 
    #print "txt=",txt
    data = []        
    #soup = BeautifulSoup(txt)
    object = re.compile('<a .*>(.*)</a>')    
    for f in txt.findAll('a'):
        #print "f=",f.getText()        
        x = object.search(str(f)).group(1)
        #print x
        if x=="...": continue
        data.append(x)
    return data




def getValue(k, v):
    k = k.strip()
    v = v.strip()
    if k == 'Оригинальное название':
        return {'orig_name': v}
    elif k == 'Страна':
        return {'country': v}
    elif k == 'Слоган':
        return {'slogan': v}
    elif k == 'Режиссер':
        return {'producer': [d.strip() for d in v.split(',')]}
    elif k == 'Актеры':
        return {'actors': [d.strip() for d in v.split(',')]}
    elif k == 'Жанр':
        return {'genre': [d.strip() for d in v.split(',')]}
    elif k == 'Премьера в мире':
        return {'dt_world': v}
    elif k == 'Дата выхода в России':
        return {'dt_russia': v}
    elif k == 'Продолжительность':
        return {'timing': v}
    elif k == 'Год':
        return {'year': v}

    return





def parseFilm(url):
    data = requests.get(url).content
    soup = BeautifulSoup(data, 'lxml')
    title = soup.find('div', {'class':'post-title'}).getText()
    info_div = soup.find('div', {'class':'post-story'}).findAll('div')[1]
    img = info_div.findAll('img')[0]
    text = "".join([str(x) for x in info_div.contents]) 
    img_src = img.get('src')
    if 'http://filmitorrent.org' not in img_src:
        img_src = 'http://filmitorrent.org' + img_src
    info = {
        'title': title,
        'img': img_src,     
        'url': url,       
    }

    # print('img=', img.get('src'))

    for t in text.split('<br/>'):
        if len(t.split(':</b>'))<2: continue
        k = t.split(':</b>')[0].replace('<b>', '')
        v = cleanhtml(t.split(':</b>')[1])
        print(k, '=', v)
        r = getValue(k, v)
        if r==None:
            continue
        info.update(r)


    if len(text.split('</object></div><br/>'))>1:
        description = text.split('</object></div><br/>')[1].split('<br/><br/>')[0]            
        info['description'] = description.replace('<br/>', ' ').replace('<br>', ' ')

    print(info.get('description'))

    return saveFilm(info)


def parseKinopoiskFilm(url):

    data = requests.get(url, headers = { 
    'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language':'ru,en-us;q=0.7,en;q=0.3',
    'Cache-Control':'max-age=0',
    'Connection':'keep-alive',
    'Cookie':'user_country=ru; my_perpages=a%3A0%3A%7B%7D; __utma=168025531.1625787791.1343102616.1355074080.1355076436.37; __utmz=168025531.1355076436.37.37.utmcsr=ftpface.ru|utmccn=(referral)|utmcmd=referral|utmcct=/file/safe_2012_dub_ustransfer_bdrip_xvid_ac3_avi.html; last_visit=2012-12-09+22%3A18%3A30; mobile=no; __utmb=168025531.5.10.1355076436; PHPSESSID=8fe12aa58fa12a34754bad22355a5c85; __utmc=168025531',
    'Host':'www.kinopoisk.ru',
    'Referer':'http://ftpface.ru/file/safe_2012.html',
    'User-Agent':'Mozilla/5.0 (X11; Linux x86_64; rv:16.0) Gecko/20100101 Firefox/16.0',
    }  ).content

    soup = BeautifulSoup(data, "html.parser") 
    img_href = soup.find("img",itemprop="image")
    if img_href==None: return 
    img_href = img_href["src"]    
    fi = {"img": img_href}
    fi["title"] = soup.findAll("h1",itemprop="name")[0].getText().replace("&nbsp;"," ")
    if u"сериал" in fi["title"]: return 
    #if u"(видео)" in fi["name"]: return 
    fi["orig_name"] = soup.find("span",{"itemprop":"alternativeHeadline"}).getText()
    try:
        fi["description"] = soup.findAll("div",itemprop="description")[0].getText()
    except: 
        fi["description"] = ""
    try:
        fi["kp_raiting"] = float(soup.find("meta",itemprop="ratingValue")['content'])
    except: 
        fi["kp_raiting"]=0
    data = []
    for t in soup.findAll("li",itemprop="actors"):
        if len(infoData(t))==0: continue
        data.append(infoData(t)[0])
        if len(data)>=5: break
    fi["actors"] = data
    
    # поиск примьера Россия
    xsoup = soup
    soup = xsoup.find("table",{"class":"info"})#BeautifulSoup(html)
    if soup==None: soup = xsoup.find("table",{"class":"info "})#BeautifulSoup(html)
    if soup==None: soup = xsoup.find("table",{"class":"info nooriginalPoster"})
    tags = soup.findAll('td')

    data = []
    for t in soup.findAll("span",itemprop="genre"):
        for d in t.findAll("a"):
            #if d.getText()==u"мультфильм": return None
            if d.getText()==u"документальный": return None
            data.append(d.getText())
    
    fi["genre"] = data
    ydata = infoData(tags[1])
    if len(ydata)==0: return None
    fi["year"]     = ydata[0]
    fi["country"]  = ", ".join(infoData(tags[3]))
    fi["producer"] = infoData(tags[7])
    if int(ydata[0])<1980: return None
    # поиск премьера Россия
    e = soup.find("td",{"id":"div_rus_prem_td2"})
    if e!=None:
        try:
            fi["dt_world"] = datetime.datetime.strptime(e.find("div",{"class":"prem_ical"})["data-date-premier-start-link"], "%Y%m%d").strftime('%d.%m.%Y')
        except:
            fi["dt_world"] = datetime.datetime(int(fi["year"]),1,1).strftime('%d.%m.%Y')
    else:
        fi["dt_world"] = datetime.datetime(int(fi["year"]),1,1).strftime('%d.%m.%Y')
    # fi["img_data"] = downloadImg(url,img_href)

    print(fi)

    return saveFilm(fi)


def saveFilm(info):
    title = info.get('title')
    if Film.objects.filter(title=title).count()>0:
        print('film exists', 'http://vmeste.tv'+Film.objects.get(title=title).get_absolute_url())
        return
    film, cr = Film.objects.get_or_create(title=title)
    genres = []

    for name in info.get('genre', []):
        g, cr = Genre.objects.get_or_create(name=name)
        genres.append(g)

    film.genre.set(genres)
    film.is_active = False
    film.info = info

    film.save()
    slug = slugify(unidecode(title))
    if Film.objects.filter(slug=slug).exists():
        slug = str(film.id) + '-' + slug
    slug = slug[:50]
    film.slug = slug
    film.save()

    r = requests.get(info['img'])

    img_temp = NamedTemporaryFile(delete=True)
    img_temp.write(r.content)
    img_temp.flush()
    film.image.save("image.jpg", File(img_temp), save=True)
    print('film id=', film.id)
    return film

def loadCommentsFT(film, url):
    soup = BeautifulSoup(requests.get(url).content, 'lxml')
    for dv in soup.find_all('div', {'class':'comment-right'}):
        for d in dv.find_all('div'):
            if 'comm-id-' not in d.get('id', ''):
                continue
            comment = d.getText()
            FilmComment.objects.create(film=film, comment=comment, is_other_site=True)    

def downloadFilm(url):
    film = None
    if 'kinopoisk' in url:
        film = parseKinopoiskFilm(url)
    else:
        film = parseFilm(url)    

    if film and 'filmitorrent' in url:
        loadCommentsFT(film, url)        
    
    return film
