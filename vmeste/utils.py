from channels.db import database_sync_to_async

from django.utils import timezone

from .exceptions import ClientError
from .models import Room, UserInRoom


def getHeader(name, scope):
    for h in scope["headers"]:
        if h[0].decode('utf-8')==name:
            return h[1].decode('utf-8')

# This decorator turns this function from a synchronous function into an async one
# we can call from our async consumers, that handles Django DBs correctly.
# For more, see http://channels.readthedocs.io/en/latest/topics/databases.html
@database_sync_to_async
def get_room_or_error(room_id, user):
    """
    Tries to fetch a room for the user, checking permissions along the way.
    """
    # Check if the user is logged in

    # print('room_id', room_id)
    #if not user.is_authenticated:
    #    raise ClientError("USER_HAS_TO_LOGIN")
    # Find the room they requested (by ID)
    try:
        room = Room.objects.get(room_uuid=room_id)
    except Room.DoesNotExist:
        raise ClientError("ROOM_INVALID")
    # Check permissions
    # if room.staff_only and not user.is_staff:
    #     raise ClientError("ROOM_ACCESS_DENIED")
    return room


@database_sync_to_async
def update_room_user_count(room_id, scope, value):
    user = scope["user"]

    try:
        room = Room.objects.get(room_uuid=room_id)

        if value>0:
            ur, cr = UserInRoom.objects.get_or_create(room=room, name=user.username, ua=getHeader('user-agent', scope), ip=getHeader('x-real-ip', scope), is_active=True)
        else:
            UserInRoom.objects.filter(room=room, name=user.username, is_active=True).update(is_active=False)

        room.user_count = UserInRoom.objects.filter(room=room, is_active=True).count()
        if room.user_count > room.max_user_count:
            room.max_user_count = room.user_count

        if room.user_count==0:
            room.dt_disconnect_all = timezone.now()
            
        room.save()
    except Room.DoesNotExist:
        print('room invalid or error in room')
        raise ClientError("ROOM_INVALID")
    # Check permissions
    # if room.staff_only and not user.is_staff:
    #     raise ClientError("ROOM_ACCESS_DENIED")
    return room


@database_sync_to_async
def update_room_open(room_id, user):
    """
    Tries to fetch a room for the user, checking permissions along the way.
    """
    # Check if the user is logged in

    # print('room_id', room_id)
    #if not user.is_authenticated:
    #    raise ClientError("USER_HAS_TO_LOGIN")
    # Find the room they requested (by ID)
    try:        
        room = Room.objects.get(room_uuid=room_id)
        room.is_open = not room.is_open
        room.save()
    except Room.DoesNotExist:
        raise ClientError("ROOM_INVALID")
    # Check permissions
    # if room.staff_only and not user.is_staff:
    #     raise ClientError("ROOM_ACCESS_DENIED")
    return room


@database_sync_to_async
def room_update_progress(room_id, user, scope, progress):
    try:   
        Room.objects.filter(room_uuid=room_id).update(progress=progress)
        UserInRoom.objects.filter(room__room_uuid=room_id, name=user.username, is_active=True).update(progress=progress)
    except Room.DoesNotExist:
        raise ClientError("ROOM_INVALID")

@database_sync_to_async
def room_update_user_progress(room_id, user, scope, progress):
    try:        
        UserInRoom.objects.filter(room__room_uuid=room_id, name=user.username, is_active=True).update(progress=progress)
    except Room.DoesNotExist:
        raise ClientError("ROOM_INVALID")


@database_sync_to_async
def room_set_pause(room_id, user, progress):
    try:        
        Room.objects.filter(room_uuid=room_id).update(is_pause=True, progress=progress)
    except Room.DoesNotExist:
        raise ClientError("ROOM_INVALID")


@database_sync_to_async
def room_set_resume(room_id, user, progress):
    try:        
        Room.objects.filter(room_uuid=room_id).update(is_pause=False, progress=progress)
    except Room.DoesNotExist:
        raise ClientError("ROOM_INVALID")


@database_sync_to_async
def room_set_seek(room_id, user, progress):
    try:        
        Room.objects.filter(room_uuid=room_id).update(progress=progress)
    except Room.DoesNotExist:
        raise ClientError("ROOM_INVALID")


@database_sync_to_async
def room_set_ready(room_id, user, ready_time, user_agent):
    try:        
        UserInRoom.objects.filter(room__room_uuid=room_id, name=user.username, is_active=True).update(ready_time=ready_time, ua=user_agent)
    except Room.DoesNotExist:
        raise ClientError("ROOM_INVALID")

