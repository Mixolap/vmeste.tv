import json
import datetime
import sys
import subprocess

from .models import VideoLink, VideoSearch

def searchVideo(source_link):
    if source_link==None or source_link.strip()=='': return
    VideoLink.objects.filter(source_link=source_link, dt_create__lte=datetime.datetime.now() - datetime.timedelta(hours=4)).delete()
    if VideoLink.objects.filter(source_link=source_link).exists(): return
    print('searching', source_link)
    data = None
    try:
        data = subprocess.Popen(['/usr/local/bin/youtube-dl', '-j', source_link], stdout = subprocess.PIPE, stderr = subprocess.STDOUT).stdout.read().decode('utf-8').strip()
        print('data=', data)
        data = json.loads(data)
    except:
        # print('nothing found', sys.exc_info()[0])
        VideoSearch.objects.create(text=source_link)
        return

    title = data.get('title')
    thumbnail = data.get('thumbnail')
    rcount = 0
    for f in data.get('formats', []):
        if f.get('ext')!='mp4':
            continue
        quality = f.get('format_note')
        if quality not in ['small', 'medium']:
            continue
        video_url = f.get('url')
        vcodec = f.get('vcodec')
        if vcodec == 'av01.0.05M.08':
            continue
        rcount += 1
        VideoLink.objects.create(source_link=source_link, title=title, thumbnail=thumbnail, quality=quality, video_url=video_url)

    VideoSearch.objects.create(text=source_link, title=title, rcount=rcount)



