from django.conf import settings

from channels.generic.websocket import AsyncJsonWebsocketConsumer

from .exceptions import ClientError
from .utils import *


class ChatConsumer(AsyncJsonWebsocketConsumer):
    """
    This chat consumer handles websocket connections for chat clients.

    It uses AsyncJsonWebsocketConsumer, which means all the handling functions
    must be async functions, and any sync work (like ORM access) has to be
    behind database_sync_to_async or sync_to_async. For more, read
    http://channels.readthedocs.io/en/latest/topics/consumers.html
    """

    ##### WebSocket event handlers

    async def connect(self):
        """
        Called when the websocket is handshaking as part of initial connection.
        """
        # Are they logged in?
        # if self.scope["user"].is_anonymous:
        #     # Reject the connection
        #     await self.close()
        # else:
        # Accept the connection
        await self.accept()
        # Store which rooms the user has joined on this connection
        self.rooms = set()

    async def receive_json(self, content):
        """
        Called when we get a text frame. Channels will JSON-decode the payload
        for us and pass it as the first argument.
        """
        # Messages will have a "command" key we can switch on
        command = content.get("command", None)
        try:
            if command == "join":
                # Make them join the room
                await self.join_room(content)
            elif command == "leave":
                # Leave the room
                await self.leave_room(content["room"])
            elif command == "send":
                await self.send_room(content["room"], content["message"])
            elif command == "change_open":
                await self.change_open_room(content["room"])
            elif command == "progress":
                await self.progress(content)
            elif command == "user_progress":
                await self.user_progress(content)
            elif command == "pause":
                await self.pause(content)
            elif command == "resume":
                await self.resume(content)
            elif command == "seek":
                await self.seek(content)
            elif command == "ready":
                await self.ready(content)
        except ClientError as e:
            # Catch any errors and send it back
            await self.send_json({"error": e.code})

    async def disconnect(self, code):
        """
        Called when the WebSocket closes for any reason.
        """
        # Leave all the rooms we are still in
        for room_id in list(self.rooms):
            try:
                await self.leave_room(room_id)
            except ClientError:
                pass

    ##### Command helper methods called by receive_json

    async def progress(self, content):
        room_id = content["room"]
        await room_update_progress(room_id, self.scope["user"], self.scope, content["progress"])
        room = await get_room_or_error(room_id, self.scope["user"])
        await self.send_json({
            "user_count": room.user_count,
            "is_pause": room.is_pause,
            "progress": room.progress,
            "users": [{"name": d.name, "ready_time": d.ready_time, "progress": d.progress} for d in room.users()]
        })  
    ##### Command helper methods called by receive_json

    async def user_progress(self, content):
        room_id = content["room"]
        room = await get_room_or_error(room_id, self.scope["user"])
        await room_update_user_progress(room_id, self.scope["user"], self.scope, content["progress"])
        await self.send_json({
            "user_count": room.user_count,
            "is_pause": room.is_pause,
            "progress": room.progress,
            "users": [{"name": d.name, "ready_time": d.ready_time, "progress": d.progress} for d in room.users()]
        })  

    async def pause(self, content):
        room_id = content["room"]
        await room_set_pause(room_id, self.scope["user"], content["progress"])
        await self.channel_layer.group_send(
                str(room_id),
                {
                    "type": "video.operation",
                    "operation": "pause",
                    "room_id": room_id,
                    "progress": content["progress"],
                }
            )

    async def resume(self, content):
        room_id = content["room"]
        await room_set_resume(room_id, self.scope["user"], content["progress"])
        await self.channel_layer.group_send(
                str(room_id),
                {
                    "type": "video.operation",
                    "operation": "resume",
                    "room_id": room_id,
                    "progress": content["progress"],
                }
            )

    async def seek(self, content):
        room_id = content["room"]

        await room_set_seek(room_id, self.scope["user"], content["progress"])
        
        await self.channel_layer.group_send(
                str(room_id),
                {
                    "type": "video.operation",
                    "operation": "seek",
                    "room_id": room_id,
                    "progress": content["progress"],
                }
            )

    async def ready(self, content):
        room_id = content["room"]
        await room_set_ready(room_id, self.scope["user"], content.get("ready_time"), content.get("user_agent"))
        # await self.channel_layer.group_send(
        #         str(room_id),
        #         {
        #             "type": "video.operation",
        #             "operation": "seek",
        #             "room_id": room_id,
        #             "progress": content["progress"],
        #         }
        #     )


    async def join_room(self, content):
        """
        Called by receive_json when someone sent a join command.
        """
        # The logged-in user is in our scope thanks to the authentication ASGI middleware
        room_id = content["room"]
        user_name = content.get("user_name")
        self.scope["user"].username = user_name
        room = await get_room_or_error(room_id, self.scope["user"])
        await update_room_user_count(room_id, self.scope, 1)
        # Send a join message if it's turned on
        if settings.NOTIFY_USERS_ON_ENTER_OR_LEAVE_ROOMS:
            await self.channel_layer.group_send(
                str(room.room_uuid),
                {
                    "type": "chat.join",
                    "room_id": room_id,
                    "username": self.scope["user"].username,
                }
            )
        # Store that we're in the room
        self.rooms.add(room_id)
        # Add them to the group so they get room messages
        await self.channel_layer.group_add(
            str(room.room_uuid),
            self.channel_name,
        )
        # Instruct their client to finish opening the room
        await self.send_json({
            "join": str(room.room_uuid),
            "title": room.title,
        })

    async def leave_room(self, room_id):
        """
        Called by receive_json when someone sent a leave command.
        """
        # The logged-in user is in our scope thanks to the authentication ASGI middleware
        room = await get_room_or_error(room_id, self.scope["user"])
        await update_room_user_count(room_id, self.scope, -1)
        # Send a leave message if it's turned on
        if settings.NOTIFY_USERS_ON_ENTER_OR_LEAVE_ROOMS:
            await self.channel_layer.group_send(
                str(room.room_uuid),
                {
                    "type": "chat.leave",
                    "room_id": room_id,
                    "username": self.scope["user"].username,
                }
            )
        # Remove that we're in the room
        self.rooms.discard(room_id)
        # Remove them from the group so they no longer get room messages
        await self.channel_layer.group_discard(
            str(room.room_uuid),
            self.channel_name,
        )
        # Instruct their client to finish closing the room
        await self.send_json({
            "leave": str(room.room_uuid),
        })

    async def send_room(self, room_id, message):
        # print('room_id', room_id)
        """
        Called by receive_json when someone sends a message to a room.
        """
        # Check they are in this room
        if room_id not in self.rooms:
            raise ClientError("ROOM_ACCESS_DENIED")
        # Get the room and send to the group about it
        room = await get_room_or_error(room_id, self.scope["user"])

        await self.channel_layer.group_send(
            str(room.room_uuid),
            {
                "type": "chat.message",
                "room_id": room_id,
                "username": self.scope["user"].username,
                "message": message,
            }
        )

    async def change_open_room(self, room_id):
        if room_id not in self.rooms:
            raise ClientError("ROOM_ACCESS_DENIED")
        # Get the room and send to the group about it
        room = await update_room_open(room_id, self.scope["user"])

        if room.is_open:
            message = "Комната открыта для других"
        else:
            message = "Комната скрыта от других"

        await self.channel_layer.group_send(
            str(room.room_uuid),
            {
                "type": "chat.message",
                "msg_type": settings.MSG_TYPE_ALERT,
                "room_id": room_id,
                "username": self.scope["user"].username,
                "message": message,
                "is_open": room.is_open,
            }
        )

    ##### Handlers for messages sent over the channel layer

    # These helper methods are named by the types we send - so chat.join becomes chat_join
    async def chat_join(self, event):
        """
        Called when someone has joined our chat.
        """
        # Send a message down to the client
        await self.send_json(
            {
                "msg_type": settings.MSG_TYPE_ENTER,
                "room": event["room_id"],
                "username": event["username"],
            },
        )

    async def chat_leave(self, event):
        """
        Called when someone has left our chat.
        """
        # Send a message down to the client
        await self.send_json(
            {
                "msg_type": settings.MSG_TYPE_LEAVE,
                "room": event["room_id"],
                "username": event["username"],
            },
        )

    async def chat_message(self, event):
        """
        Called when someone has messaged our chat.
        """
        # Send a message down to the client
        await self.send_json(
            {
                "msg_type": settings.MSG_TYPE_MESSAGE,
                "room": event["room_id"],
                "username": event["username"],
                "message": event["message"],
            },
        )

    async def video_operation(self, event):
        await self.send_json(
            {
                "msg_type": settings.MSG_TYPE_MESSAGE,
                "room": event["room_id"],
                "operation": event["operation"],
                "progress": event["progress"],
            },
        )
