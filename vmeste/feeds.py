from yaturbo import YandexTurboFeed
from django.urls import reverse
from .models import Film
from django.template.defaultfilters import *

class TurboFeed(YandexTurboFeed):
    title = "vmeste.tv - смотрите фильмы вместе"
    link = "http://vmeste.tv"
    description = "Смотрите фильмы вместе с друзьями онлайн, скопировав им ссылку на комнату без регистрации на сайте и установки программ. Приятного совместного просмотра онлайн."
    turbo_sanitize = True

    def items(self):
        return Film.objects.filter(is_hidden=False, is_active=True).order_by('-id')[:200]

    def item_title(self, item):
        return item.title

    def item_turbo(self, item):
        if item.image:
            text = '<img src="http://vmeste.tv/%s"><br/>'%(item.image.url)
        else:
            text = '<img src="%s"><br/>'%(item.image_url())
        text += '<p>Жанр: ' + ', '.join(item.info.get('genre')) + '</p>'
        text += '<p>Режисёр: ' + ', '.join(item.info.get('producer')) + '</p>'
        text += '<p>Актёры: ' + ', '.join(item.info.get('actors')) + '</p>'
        text += '<a href="http://vmeste.tv' + item.get_absolute_url() + '">Смотреть вместе на vmeste.tv</a>'
        text += '<h2>Отзывы</h2><ul>'
        for comment in item.other_comments():
            text += '<li>' + comment.comment + '</li>'
        text += '</ul>'

        return text

    # item_link is only needed if NewsItem has no get_absolute_url method.
    def item_link(self, item):
        return 'http://vmeste.tv'+item.get_absolute_url()


feed = TurboFeed()

# configure Yandex Metrika counter
feed.configure_analytics_yandex('50043883')