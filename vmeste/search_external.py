import json
import requests

from .models import ExternalSearch, ExternalFilm

api_token = 'api_token'

def searchExternal(text):
    if ExternalSearch.objects.filter(text=text).exists():
        return ExternalSearch.objects.filter(text=text)[0].results

    params = {
        'api_token': api_token,
        'title': text,
    }
    url = 'http://moonwalk.cc/api/videos.json'
    data = []
    try:
        data = json.loads(requests.get(url, params=params).content.decode('utf-8'))
    except:
        ExternalSearch.objects.create(text=text, is_success=False)
        return {}

    # print('data=', data)
    try:
        error = data.get('error')
        ExternalSearch.objects.create(text=text, raw_data=data, results={})
        return {}
    except:
        pass

    results = {}
    for d in data:                
        material_data = d.get('material_data')
        if material_data==None:
            continue
        film = {
            'title': d.get('title_ru'),
            'year': d.get('year'),
            'source_type': d.get('source_type'),
            'token': d.get('token'),
            'iframe_url': d.get('iframe_url'),
            'translator': d.get('translator'),
            'poster': material_data.get('poster'),
            'description': material_data.get('description'),
        }

        vtype = d.get('type')
        if vtype in results.keys():
            results[vtype].append(film)
        else:
            results[vtype] = [film]
        ExternalFilm.objects.get_or_create(token=film['token'], defaults={'title': film['title'], 'info':film})

    ExternalSearch.objects.create(text=text, raw_data=data, results=results, rcount=len(data))
    return results
        

