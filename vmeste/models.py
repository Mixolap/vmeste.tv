import datetime
import uuid

from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.core.files.storage import FileSystemStorage
from django.db import connection
from django.db.models.signals import post_save

fs = FileSystemStorage(location=settings.STATIC_ROOT)

def create_profile(sender, **kwargs):
    user = kwargs["instance"]
    if kwargs["created"]:
        UserProfile.objects.create(user=user)

post_save.connect(create_profile, sender=User)


class UserProfile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	total_size = models.IntegerField(default=0)
	name = models.CharField(max_length=2048, null=True, blank=True)
	vk_link = models.CharField(max_length=2048, null=True, blank=True)
	ok_link = models.CharField(max_length=2048, null=True, blank=True)
	is_show_contacts = models.BooleanField(default=True)


class Genre(models.Model):
	name = models.CharField(max_length=1024)
	name2 = models.CharField(max_length=1024, null=True, blank=True)
	name3 = models.CharField(max_length=1024, null=True, blank=True)
	on_main = models.BooleanField(default=False)
	slug = models.SlugField()
	
	def __str__(self):
		return self.name

	def films(self):
		return Film.objects.filter(genre=self, is_active=True)

	def popular_films(self):
		return Film.objects.filter(genre=self, is_active=True)[:4]

	def get_absolute_url(self):
		return '/genre/%s/'%(self.slug)


class Film(models.Model):
	title = models.CharField(max_length=1024)
	genre = models.ManyToManyField(Genre)
	is_active = models.BooleanField(default=False)
	description = models.TextField(null=True, blank=True)
	image     = models.ImageField(upload_to='static/images/%Y', storage=fs, blank=True, null=True)
	image_src = models.CharField(max_length=2048, blank=True, null=True)
	is_external = models.BooleanField(default=False)
	info   = JSONField(null=True, blank=True)	
	dt_create = models.DateTimeField(auto_now_add=True)
	server = models.CharField(max_length=50, null=True, blank=True, default=settings.DEFAULT_UPLOAD_SERVER, verbose_name='Сервер')
	slug = models.SlugField(null=True, blank=True)
	is_hidden = models.BooleanField(default=False)
	is_hls = models.BooleanField(default=False)
	np = models.IntegerField(default=1000)
	other_films = models.ManyToManyField("self", blank=True)

	def get_absolute_url(self):
		return '/film/%s/'%(self.slug)

	def get_edit_absolute_url(self):
		return '/film/%s/edit'%(self.slug)

	class Meta:
		ordering = ('-np', '-id', )

	def __str__(self):
		return self.title

	def image_url(self):
		if self.is_external:
			return self.image_src
		return '/'+self.image.url

	def get_location(self):
		server = '//vmeste.tv'
		if self.server!=None:
			server = '//'+self.server
		if self.is_hls:
			return '//' + self.server + '/mp4/' + str(self.pk) + '.m3u8'
		return server+'/static/mp4/' + str(self.pk) + '.mp4'

	def get_video_type(self):
		if self.is_hls:
			return 'application/x-mpegurl'
		return 'video/mp4'

	def other_comments(self):
		return FilmComment.objects.filter(film=self, is_hide=False, is_other_site=True)

	def comments(self):
		return FilmComment.objects.filter(film=self, is_hide=False, is_other_site=False)

	def get_other_films(self):
		count = 6
		cur = connection.cursor()		
		cur.execute("select r.film_id, count(*) as x from vmeste_room r join vmeste_film_genre fg on r.film_id=fg.film_id left join vmeste_film f on r.film_id=f.id where f.is_hidden=false and f.is_active=true and max_user_count > 0 and genre_id in (select genre_id from vmeste_film_genre where film_id=%d) and f.id<>%d and r.dt_create>=current_timestamp - interval '10 days' group by r.film_id order by x desc limit %d"%(self.pk, self.pk, count))
		ids = [c[0] for c in cur]
		data = []
		for id in ids:
			data.append(Film.objects.get(pk=id))
		return data

	def get_description(self):
		if self.info.get('description'):
			return self.info.get('description')
		return self.description or ''


class ExternalFilm(models.Model):
	title    = models.CharField(max_length=1024)
	token    = models.CharField(max_length=1024)
	info     = JSONField(null=True, blank=True)

	def get_absolute_url(self):
		return '/search/?text=' + self.title


class VideoSearch(models.Model):
	text = models.CharField(max_length=2048, null=True, blank=True)
	title = models.CharField(max_length=2048, null=True, blank=True)
	rcount = models.IntegerField(default=0)
	dt_create = models.DateTimeField(auto_now_add=True)

class VideoLink(models.Model):
	source_link = models.CharField(max_length=2048, null=True, blank=True)
	title = models.CharField(max_length=2048, null=True, blank=True)
	video_url = models.TextField(null=True, blank=True)
	quality = models.CharField(max_length=20, null=True, blank=True)
	thumbnail = models.CharField(max_length=2048, null=True, blank=True)
	info = JSONField(null=True, blank=True)
	dt_create = models.DateTimeField(auto_now_add=True)


class Room(models.Model):
	room_uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
	author_uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=False)
	title = models.CharField(max_length=2048, null=True, blank=True)
	film = models.ForeignKey(Film, null=True, blank=True, on_delete=models.SET_NULL)
	external_film = models.ForeignKey(ExternalFilm, null=True, blank=True, on_delete=models.SET_NULL)
	is_active = models.BooleanField(default=True)
	dt_create = models.DateTimeField(auto_now_add=True)
	dt_disconnect_all = models.DateTimeField(null=True, blank=True)
	is_open = models.BooleanField(default=False)
	user_count = models.IntegerField(default=0)
	max_user_count = models.IntegerField(default=0)
	progress = models.FloatField(default=0)
	is_pause = models.BooleanField(default=True)
	is_private = models.BooleanField(default=False)
	room_uuid_hex = models.CharField(max_length=255, null=True, blank=True)
	user_agent = models.TextField(null=True, blank=True)
	ip = models.GenericIPAddressField(null=True, blank=True)
	video_url = models.CharField(max_length=2048, null=True, blank=True)
	
	def __str__(self):
		if not self.title: return self.film.title
		return self.title

	def get_video_type(self):
		if self.film and self.film.is_hls:
			return 'application/x-mpegurl'
		if self.is_private:
			return 'application/x-mpegurl'
		return 'video/mp4'

	def users(self):
		return UserInRoom.objects.filter(room=self, is_active=True)

	def get_uuid(self):
		return self.room_uuid.hex

	def get_absolute_url(self):
		if self.film:
			return '/show/%s/%s'%(self.film.slug, self.room_uuid)
		elif self.external_film:
			return '/view/%s/%s'%(self.external_film.token, self.room_uuid)

		return '/video/%s'%(self.room_uuid)

	def current_time(self):		
		return datetime.datetime.utcfromtimestamp(self.progress).strftime('%H:%M:%S')

	def get_location(self):
		if self.film:
			return self.film.get_location()
		return self.video_url

	def get_source_url(self):
		if self.film:
			return self.film.get_absolute_url()
		return 'http://vmeste.tv'

	# def get_absolute_url(self):
	# 	if self.film:
	# 		return 'http://vmeste.tv/show/' + self.film.slug + '/' + self.get_uuid()
	# 	return 'http://vmeste.tv/video/' + self.get_uuid()


class UserInRoom(models.Model):
	room = models.ForeignKey(Room, on_delete=models.CASCADE)
	name = models.CharField(max_length=255)
	is_active = models.BooleanField(default=True)
	ready_time = models.FloatField(default=0)
	progress = models.FloatField(default=0)
	ua = models.TextField(null=True, blank=True)
	ip = models.GenericIPAddressField(null=True, blank=True)
	dt_create = models.DateTimeField(auto_now_add=True)
	dt_update = models.DateTimeField(auto_now=True)

	def current_time(self):		
		return datetime.datetime.utcfromtimestamp(self.progress).strftime('%H:%M:%S')

	def ip_location(self):
		from django.contrib.gis.geoip2 import GeoIP2
		g = GeoIP2()
		try:
			city = g.city(self.ip)
		except:
			return 'unknown'

		town = city.get('city') or 'unknown'
		ctry = city.get('country_code') or 'unknown'
		return  ctry + ' - ' + town

class Search(models.Model):
	text = models.TextField(null=True, blank=True)
	rcount = models.IntegerField(default=0)
	dt_create = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.text


class ErrorJS(models.Model):
	body = models.TextField()	
	dt_create = models.DateTimeField(auto_now=True)


class Metrics(models.Model):
	data = JSONField(null=True, blank=True)	
	dt_create = models.DateTimeField(auto_now=True)


class FilmComment(models.Model):
	film = models.ForeignKey(Film, on_delete=models.CASCADE)
	is_other_site = models.BooleanField(default=False)
	is_hide = models.BooleanField(default=False)
	rate = models.IntegerField(default=0)
	username = models.CharField(max_length=1024, null=True, blank=True)
	comment = models.TextField()
	dt_create = models.DateTimeField(auto_now=True)


class Feedback(models.Model):
	text = models.TextField(null=True, blank=True)
	dt_create = models.DateTimeField(auto_now=True)


class Serial(models.Model):
	title = models.CharField(max_length=2048)
	description = models.TextField(null=True, blank=True)
	torrent_link = models.CharField(max_length=2048, null=True, blank=True)
	image = models.ImageField(upload_to='static/images/%Y', storage=fs, blank=True, null=True)
	info   = JSONField(null=True, blank=True)	
	is_downloaded = models.BooleanField(default=False)
	is_hidden = models.BooleanField(default=True)
	slug = models.SlugField(null=True, blank=True)


class Season(models.Model):
	serial = models.ForeignKey(Serial, on_delete=models.CASCADE)
	number = models.IntegerField()
	title = models.CharField(max_length=2048)
	description = models.TextField(null=True, blank=True)
	torrent_link = models.CharField(max_length=2048, null=True, blank=True)
	image = models.ImageField(upload_to='static/images/%Y', storage=fs, blank=True, null=True)
	info   = JSONField(null=True, blank=True)	
	is_downloaded = models.BooleanField(default=False)
	is_hidden = models.BooleanField(default=False)


class Episode(models.Model):
	season = models.ForeignKey(Season, on_delete=models.CASCADE)
	number = models.IntegerField()
	title = models.CharField(max_length=2048)
	description = models.TextField(null=True, blank=True)
	torrent_link = models.CharField(max_length=2048, null=True, blank=True)
	server = models.CharField(max_length=50, null=True, blank=True, default=settings.DEFAULT_UPLOAD_SERVER, verbose_name='Сервер')
	image = models.ImageField(upload_to='static/images/%Y', storage=fs, blank=True, null=True)
	info   = JSONField(null=True, blank=True)	
	is_downloaded = models.BooleanField(default=False)
	is_hidden = models.BooleanField(default=False)


class ExternalSearch(models.Model):
	text = models.TextField()
	raw_data = JSONField(null=True, blank=True)
	results = JSONField(null=True, blank=True)
	rcount = models.IntegerField(default=0)
	is_success = models.BooleanField(default=True)
	dt_create = models.DateTimeField(auto_now_add=True)


class FilmCollection(models.Model):
	title_short = models.CharField(max_length=1024)
	title = models.CharField(max_length=1024, null=True, blank=True)
	text = models.TextField(null=True, blank=True)
	films = models.ManyToManyField(Film, blank=True)
	films_external = models.ManyToManyField(ExternalFilm, blank=True)
	np = models.IntegerField(default=10)
	is_active = models.BooleanField(default=False)
	is_filling = models.BooleanField(default=True)
	slug = models.SlugField(null=True, blank=True)
	
	def get_absolute_url(self):
		return '/collection/%s/'%(self.slug)

class Synonym(models.Model):
	text1 = models.CharField(max_length=1024)
	text2 = models.CharField(max_length=1024)
	
class UserVideo(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	title = models.CharField(max_length=2048, null=True, blank=True)
	description = models.TextField(null=True, blank=True)
	file = models.FileField(upload_to='static/uploads/%Y', storage=fs, blank=True, null=True)
	file_name = models.CharField(max_length=2048, null=True, blank=True)
	is_removed = models.BooleanField(default=False)
	is_active = models.BooleanField(default=False)
	is_process = models.BooleanField(default=False)
	dt_create = models.DateTimeField(auto_now_add=True)
	dt_updated = models.DateTimeField(auto_now=True)
	dt_process_start = models.DateTimeField(null=True, blank=True)
	dt_process_end = models.DateTimeField(null=True, blank=True)
	server = models.CharField(max_length=2048, null=True, blank=True)
	size = models.IntegerField(default=0)

	def get_location(self):
		if self.server!=None:
			return '//' + self.server + '/mp4/' + str(self.pk) + '_user.m3u8'
		return '/mp4/' + str(self.pk) + '_user.m3u8'


class UserShow(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	room = models.ForeignKey(Room, on_delete=models.CASCADE)
	dt_create = models.DateTimeField(auto_now_add=True)


class UserWantShow(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	film = models.ForeignKey(Film, on_delete=models.CASCADE, null=True, blank=True)	
	external_film = models.ForeignKey(ExternalFilm, null=True, blank=True, on_delete=models.CASCADE)
	is_open = models.BooleanField(default=False)
	dt_create = models.DateTimeField(auto_now_add=True)
