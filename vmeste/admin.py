from django.contrib import admin
from .models import *


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    list_display=["id", "title", "dt_create", "user_count", "max_user_count", "is_pause", "progress", "is_open", "dt_disconnect_all", "room_uuid"]
    list_display_links=["id", "title", "room_uuid", ]
    list_filter = ['max_user_count', 'is_open', 'is_pause']
    search_fields=['room_uuid', 'room_uuid_hex']
    def view_on_site(self, obj):
        return 'http://vmeste.tv' + obj.get_absolute_url()



admin.site.register(
    UserInRoom,
    list_display=["id", "room", "name", "dt_create", "dt_update", "is_active", "progress", "ready_time", "ua"],
    list_display_links=["id", "name"],
    list_filter = ['is_active']
)


admin.site.register(
    VideoSearch,
    list_display=["text", "rcount", "dt_create"],
    search_fields=['text'],
)


admin.site.register(Search, list_display=["text", "rcount", "dt_create"], search_fields=['text'])


admin.site.register(
    Genre,
    list_display=["id", "name", "on_main"],
    list_display_links=["id", "name"],
)


@admin.register(Film)
class FilmAdmin(admin.ModelAdmin):
    list_display = ["id", "title", "is_active", "is_hidden", "dt_create", "server"]
    list_display_links = ["id", "title"]
    search_fields=['title',]
    list_filter = ['server', 'is_hidden', 'is_active', 'is_hls']
    def view_on_site(self, obj):
        return 'http://vmeste.tv' + obj.get_absolute_url() + '/edit'


@admin.register(ErrorJS)
class ErrorJSAdmin(admin.ModelAdmin):
    list_display  = ["dt_create", "body"]


@admin.register(Metrics)
class MetricsAdmin(admin.ModelAdmin):
    list_display  = ["dt_create", "data"]
    search_fields = ['data',]


@admin.register(FilmComment)
class FilmCommentAdmin(admin.ModelAdmin):
    list_display  = ["dt_create", "film", "username", "is_other_site", "comment"]
    search_fields = ["username", "comment", "film"]
    list_filter = ["is_other_site", "is_hide", "film"]


@admin.register(Feedback)
class FeedbackAdmin(admin.ModelAdmin):
    list_display  = ["dt_create", "text"]


@admin.register(ExternalSearch)
class ExternalSearchAdmin(admin.ModelAdmin):
    list_display  = ["text", "rcount", "dt_create"]


@admin.register(FilmCollection)
class FilmCollectionAdmin(admin.ModelAdmin):
    list_display  = ["title_short", "title", "is_active", "is_filling"]
    def view_on_site(self, obj):
        return 'http://vmeste.tv' + obj.get_absolute_url()


# @admin.register(Serial)
# class SerialAdmin(admin.ModelAdmin):
#     list_display  = ["title", "is_downloaded", "is_hidden"]


# @admin.register(Season)
# class SeasonAdmin(admin.ModelAdmin):
#     list_display  = ["number", "title", "is_downloaded", "is_hidden"]


# @admin.register(Episode)
# class EpisodeAdmin(admin.ModelAdmin):
    # list_display  = ["number", "title", "is_downloaded", "is_hidden"]

@admin.register(Synonym)
class SynonymAdmin(admin.ModelAdmin):
    list_display  = ["text1", "text2"]
    search_fields = ["text1", "text2"]


@admin.register(ExternalFilm)
class ExternalFilmAdmin(admin.ModelAdmin):
    list_display  = ["title", "token"]
    search_fields = ["title", "token"]


@admin.register(VideoLink)
class VideoLinkAdmin(admin.ModelAdmin):
    list_display  = ["title", "quality", "source_link"]
    search_fields = ["title", "quality", "source_link"]

@admin.register(UserVideo)
class UserVideoAdmin(admin.ModelAdmin):
    list_display  = ["title", "file_name", "volume", "user", "is_active", "is_process", "dt_create", "dt_process_start", "dt_process_end", "server", "is_removed"]
    search_fields = ["title", "file_name", "user"]
    list_filter = ["user", "is_active", "is_process", "is_removed"]
