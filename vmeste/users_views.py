from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.core.files.temp import NamedTemporaryFile
from django.core.files import File
from .models import *


class ProfileView(TemplateView):
    def get(self, request, *args, **kwargs):
        hide_metrika = True
        videos = UserVideo.objects.filter(is_removed=False).order_by('-id')
        return render(request, self.template_name, locals())


class UploadView(TemplateView):
    def post(self, request, *args, **kwargs):
        
        for f in request.FILES:
            file = request.FILES[f]
            print(file.name, file.size)

            uv = UserVideo.objects.create(user=request.user, title=file.name, file_name=file.name, volume=file.size)
            
            video_temp = NamedTemporaryFile(delete=True)
            for chunk in file.chunks():
                video_temp.write(chunk)
            video_temp.flush()

            uv.file.save(file.name, File(video_temp), save=True)


        return HttpResponseRedirect('/accounts/profile')



class UserVideoView(TemplateView):
    def get(self, request, *args, **kwargs):
        if kwargs.get('video_id'):
            video = UserVideo.objects.get(pk=kwargs.get('video_id'))
            room = Room.objects.create(title=video.title, video_url=video.get_location(), is_private=True)
            request.session['author_uuid'] = room.author_uuid.hex
            room.room_uuid_hex = str(room.room_uuid.hex)
            room.user_agent = request.META['HTTP_USER_AGENT']
            room.ip = request.META['REMOTE_ADDR']
            room.save()         
            return HttpResponseRedirect(room.get_absolute_url())

        if kwargs.get('room_id'):
            room = Room.objects.get(room_uuid=kwargs['room_id'])
            is_author = request.session.get('author_uuid')==room.author_uuid.hex
            return render(request, 'room.html', locals())

        return render(request, self.template_name, locals())

