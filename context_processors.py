import datetime

from vmeste.models import *
from django.conf import settings

def vars(request):
    genres = Genre.objects.filter(on_main=True)
    open_rooms = Room.objects.filter(user_count__gt=0, is_private=False, is_open=True, dt_create__gte=datetime.datetime.now()-datetime.timedelta(hours=4)).order_by("-id")[:10]
    closed_rooms = Room.objects.filter(user_count__gt=0, is_private=False, is_open=False, dt_create__gte=datetime.datetime.now()-datetime.timedelta(hours=4)).order_by("-id")[:10]
    collections = FilmCollection.objects.filter(is_active=True)
    last_search = Search.objects.filter(rcount__gt=0).order_by('-id')[:10]

    return {
        "genres": genres,
        "collections": collections,
        "open_rooms": open_rooms,
        "closed_rooms": closed_rooms,
        "last_search": last_search,
        "DEBUG": settings.DEBUG,
    }