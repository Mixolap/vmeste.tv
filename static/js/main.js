$(document).ready(function(){
	$('.load_films').click(function(){
		var cnt = $('#loaded_films').val();
		$.get('?start=' + cnt, function(data){
			$('.films').append(data);
			$('#loaded_films').val($('.film-block').length);
			if($('#loaded_films').val() == $('#total_films').val()){
				$('.load_films').hide();
			}
		})
		return false;
	});

	var o = $("#back-top");
	$(window).scroll(function(){
		if($(this).scrollTop()>100){
			o.show()
		}else{
			o.hide()}
		});
	
	o.find("a").click(function(){
		var body = $("html, body");
		body.stop().animate({scrollTop:0}, 500, 'swing');
		return false
	});

});
