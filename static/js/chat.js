function randomName(){
	var names = ['Брэд Питт', 'Орландо Блум', 'Вуди Харрельсон', 'Эмилия Кларк', 'Джонни Депп', 'Николас Кейдж', 'Дэниэл Рэдклифф', 'Эмма Уотсон', 'Анджелина Джоли', 'Дженнифер Энистон', 'Джессика Альба', 'Меган Фокс'];
	return names[Math.floor(Math.random() * names.length)];
}

function setRamdomName(){
	$('#your_name').val(randomName());
	$("#your_name").select();
	$("#your_name").focus();	
}


function showModal() {
	//Cancel the link behavior
	// e.preventDefault();

	var id = '#modal-dialog';

	//Get the screen height and width
	var maskHeight = $(document).height();
	var maskWidth = $(window).width();

	//Set height and width to mask to fill up the whole screen
	$('#mask').css({'width':maskWidth, 'height':maskHeight});
	
	//transition effect		
	$('#mask').fadeIn(500);	
	$('#mask').fadeTo("slow",0.8);	

	//Get the window height and width
	var winH = $(window).height();
	var winW = $(window).width();
          
	//Set the popup window to center
	$(id).css('top',  winH/2-$(id).height()/2);
	$(id).css('left', winW/2-$(id).width()/2);

	//transition effect
	$(id).fadeIn(1000); 

	var name = Cookies.get('name');
	if(name){
		$("#your_name").val(name);
	} else {
		
		$("#your_name").val(randomName());
	}

	$("#your_name").select();
	$("#your_name").focus();
}


function copyToClipboard(){
  var copyTextarea = document.querySelector('#link');
  copyTextarea.focus();
  copyTextarea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    if (successful){
    	$('#copied').fadeIn(500);
    	$('#copied').fadeTo("slow", 0.8);
    	setTimeout("$('#copied').fadeOut(500);", 3000)
    }
    console.log('Copying text command was ' + msg);
  } catch (err) {
    console.log('Oops, unable to copy');
  }
}	

$(document).ready(function(){	
	//if close button is clicked
	$('.window .close').click(function (e) {
		//Cancel the link behavior
		e.preventDefault();
		$('#mask, .window').hide();
	});		
});