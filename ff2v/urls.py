"""ff2v URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.views.generic.base import TemplateView
from django.contrib.auth import views as auth_views
from django.urls import path
from vmeste.feeds import feed
from vmeste.views import *
from vmeste import users_views

urlpatterns = [
    path('', Home.as_view(template_name='home.html'), name='home'),
    path('view/<str:token>/', RoomExternalView.as_view(template_name='room_external.html'), name='external_video'),
    path('view/<str:token>/<str:room_id>', RoomExternalView.as_view(template_name='room_external.html'), name='external_video'),
    path('show/<str:film_id>', RoomView.as_view(template_name='home.html'), name='new_room'),
    path('show/<str:film_id>/<str:room_id>', RoomView.as_view(template_name='room.html'), name='show_room'),
    path('video/<int:video_id>', YoutubeView.as_view(template_name='home.html'), name='new_youtube_room'),
    path('video/<str:room_id>', YoutubeView.as_view(template_name='home.html'), name='new_youtube_room'),

    path('tube/<int:video_id>', users_views.UserVideoView.as_view(template_name='home.html'), name='new_user_room'),
    path('tube/<str:room_id>', users_views.UserVideoView.as_view(template_name='home.html'), name='youtube_room'),

    path('progress/<str:room_id>', MasterProgressView.as_view(), name='progress'),
    path('film/<slug:slug>/', FilmView.as_view(template_name='film.html'), name='film'),
    path('genre/<slug:slug>/', GenreView.as_view(template_name='genre.html'), name='genre'),
    path('comment/<slug:slug>/', CommentView.as_view(template_name='comment.html'), name='comment'),
    path('collection/<slug:slug>/', FilmCollectionView.as_view(template_name='film_collection.html'), name='film_collection'),
    path('search/', SearchView.as_view(template_name='search.html'), name='search'),
    path('save/error/', ErrorView.as_view(), name='save_error'),
    path('save/metrics/', MetricsView.as_view(), name='save_metrics'),
    path('feedback/', FeedbackView.as_view(template_name='feedback.html'), name='feedback'),
    path('pravo/', TemplateView.as_view(template_name='pravo.html'), name='pravo'),
    path('youtube/', YoutubeView.as_view(template_name='videolink.html'), name='youtube'),

    path('film/<slug:slug>/edit', login_required(FilmEditView.as_view(template_name='film_edit.html')), name='film'),
    path('chats/', login_required(ChatView.as_view(template_name='chats.html')), name='chats'),
    path('stat/', login_required(StatView.as_view(template_name='stat.html')), name='stat'),
    # path('add/film/', login_required(AddFilmView.as_view(template_name='film_add.html')), name='add_film'),
    path('add/film/', AddFilmView.as_view(template_name='film_add.html'), name='add_film'),
    path('enable/<int:pk>/', EnableFilmView.as_view(), name='enable_film'),
    path('convert/film/', GetConvertView.as_view(), name='get_convert'),

    path('feeds/turbo/', feed),

    path('accounts/login/', auth_views.LoginView.as_view()),
    path('accounts/logout/', auth_views.LogoutView.as_view(next_page='/')),
    path('accounts/profile/', login_required(users_views.ProfileView.as_view(template_name='profile.html')), name='profile'),
    path('accounts/upload/', login_required(users_views.UploadView.as_view(template_name='profile.html')), name='upload'),
    
    path('admin/', admin.site.urls),
]
